﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    /// <summary>
    /// Chat service implementation.
    /// </summary>
    public class ChatService : IChatService
    {
        IUnitOfWork<MessengerContext> unitOfWork { get; set; }

        /// <summary>
        /// Contructor initialize UnitOfWork.
        /// </summary>
        /// <param name="unitOfWorkInput">Library context unit of work object.</param>
        public ChatService(IUnitOfWork<MessengerContext> unitOfWorkInput)
        {
            unitOfWork = unitOfWorkInput;
        }

        /// <summary>
        /// Creates chat using existing contact.
        /// </summary>
        /// <remarks>
        /// One chat corresponds to one contact.
        /// </remarks>
        /// <param name="contactId">Contact id for chat creating.</param>
        public async Task CreateChatByContactId(int contactId)
        {
            var Contact = await unitOfWork.GetRepository<Contact>().GetAsync(contactId);
            await Task.Run(() => unitOfWork.GetRepository<Chat>().Create(new Chat { Contact = Contact }));
            await unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes chat by id (soft delete).
        /// </summary>
        /// <remarks>
        /// Contact will not deleted.
        /// </remarks>
        /// <param name="chatId">Chat id to delete.</param>
        public async Task DeleteChatById(int chatId)
        {
            var chatToDelete = await Task.Run(() => unitOfWork.context.Chats
                  .Where(chat => chat.ChatId == chatId)
                  .FirstOrDefault()
              );

            chatToDelete.DeletedState = true;
            await unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// Get chat messages by id.
        /// </summary>
        /// <param name="chatId">Chat id.</param>
        /// <returns>List of MessageDTOs.</returns>
        public async Task<IEnumerable<MessageDTO>> GetMessagesByChatIdAsync(int chatId)
        {
            List<MessageDTO> messages = new List<MessageDTO>();
            foreach (var message in unitOfWork.context.Messages)
            {
                var currentText = await Task.Run(() => unitOfWork.context.Messages
                    .Where(m => m.Chat.ChatId == chatId)
                    .Where(m => m.MessageId == message.MessageId)
                    .Select(m => m.Text)
                    .SingleOrDefault());

                var currentTime = await Task.Run(() => unitOfWork.context.Messages
                    .Where(m => m.Chat.ChatId == chatId)
                    .Where(m => m.MessageId == message.MessageId)
                    .Select(m => m.Date)
                    .SingleOrDefault());

                var currentSenderId = await Task.Run(() => unitOfWork.context.Messages
                   .Where(m => m.Chat.ChatId == chatId)
                   .Where(m => m.MessageId == message.MessageId)
                   .Select(m => m.MassageSenderId)
                   .SingleOrDefault());

                if (currentText != null)
                    messages.Add(new MessageDTO
                    {
                        Text = currentText,
                        Date = currentTime.ToString(),
                        MassageSenderId = currentSenderId,
                        MessageId = message.MessageId
                    });
            }
            return messages;
        }

        /// <summary>
        /// Get all posible chats of concrete user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <returns>List of ChatDTOs.</returns>
        public async Task<IEnumerable<ChatDTO>> GetChatsAsync(string userId)
        {
            List<ChatDTO> chats = new List<ChatDTO>();

            foreach (var chat in unitOfWork.context.Chats)
            {
                var ContactName = await Task.Run(() => unitOfWork.context.Chats
                    .Where(chatelem => chatelem.Contact.MessengerUser.Id == userId)
                    .Where(chatelem => chatelem.ChatId == chat.ChatId)
                    .Where(chatelem => chatelem.DeletedState == false)
                    .Select(chatelem => chatelem.Contact.MessengerUserContact.UserProfile.Name)
                    .SingleOrDefault());

                if (ContactName == null)
                {
                    ContactName = await Task.Run(() => unitOfWork.context.Chats
                        .Where(chatelem => chatelem.Contact.MessengerUserContact.Id == userId)
                        .Where(chatelem => chatelem.ChatId == chat.ChatId)
                        .Where(chatelem => chatelem.DeletedState == false)
                        .Select(chatelem => chatelem.Contact.MessengerUser.UserProfile.Name)
                        .SingleOrDefault());
                }

                var LastMassageText = await Task.Run(() => unitOfWork.context.Messages
                    .Where(m => m.Chat.ChatId == chat.ChatId)
                    .Select(m => m.Text)
                    .FirstOrDefault());

                var LastMassageDate = await Task.Run(() => unitOfWork.context.Messages
                    .Where(m => m.Chat.ChatId == chat.ChatId)
                    .Select(m => m.Date.ToString().Substring(0, 11))
                    .FirstOrDefault());

                if (ContactName != null)
                    chats.Add(new ChatDTO
                    {
                        ContactName = ContactName,
                        ConcreteUserId = userId,
                        ChatId = chat.ChatId,
                        LastMassageText = LastMassageText,
                        LastMassageDate = LastMassageDate,
                    });
            }
            return chats;
        }

        /// <summary>
        /// Get deleted(soft deleted) chats by user id.
        /// </summary>
        /// <param name="userId">Concrete user id.</param>
        /// <returns>List of ChatDTOs.</returns>
        public async Task<IEnumerable<ChatDTO>> GetAvailableForAddingChatsAsync(string userId)
        {
            List<ChatDTO> chats = new List<ChatDTO>();

            foreach (var chat in unitOfWork.context.Chats)
            {
                var ContactName = await Task.Run(() => unitOfWork.context.Chats
                    .Where(chatelem => chatelem.Contact.MessengerUser.Id == userId)
                    .Where(chatelem => chatelem.ChatId == chat.ChatId)
                    .Where(chatelem => chatelem.DeletedState == true)
                    .Select(chatelem => chatelem.Contact.MessengerUserContact.UserProfile.Name)
                    .SingleOrDefault());

                if (ContactName == null)
                {
                    ContactName = await Task.Run(() => unitOfWork.context.Chats
                        .Where(chatelem => chatelem.Contact.MessengerUserContact.Id == userId)
                        .Where(chatelem => chatelem.ChatId == chat.ChatId)
                        .Where(chatelem => chatelem.DeletedState == true)
                        .Select(chatelem => chatelem.Contact.MessengerUser.UserProfile.Name)
                        .SingleOrDefault());
                }

                var LastMassageText = await Task.Run(() => unitOfWork.context.Messages
                    .Where(m => m.Chat.ChatId == chat.ChatId)
                    .Select(m => m.Text)
                    .FirstOrDefault());

                var LastMassageDate = await Task.Run(() => unitOfWork.context.Messages
                    .Where(m => m.Chat.ChatId == chat.ChatId)
                    .Select(m => m.Date.ToString().Substring(0, 11))
                    .FirstOrDefault());

                if (ContactName != null)
                    chats.Add(new ChatDTO
                    {
                        ContactName = ContactName,
                        ConcreteUserId = userId,
                        ChatId = chat.ChatId,
                        LastMassageText = LastMassageText,
                        LastMassageDate = LastMassageDate,
                    });
            }
            return chats;
        }

        /// <summary>
        /// Get chat information by chatDTO.
        /// </summary>
        /// <param name="chatDTO">Uses only ChatId and ConcreteUserId.</param>
        /// <returns>Filled ChatDTO.</returns>
        public async Task<ChatDTO> GetChat(ChatDTO chatDTO)
        {
            var ContactName = await Task.Run(() => unitOfWork.context.Chats
                .Where(chatelem => chatelem.Contact.MessengerUserContact.Id == chatDTO.ConcreteUserId)
                .Where(chatelem => chatelem.ChatId == chatDTO.ChatId)
                .Select(chatelem => chatelem.Contact.MessengerUser.UserProfile.Name)
                .SingleOrDefault());

            if (ContactName == null)
            {
                ContactName = await Task.Run(() => unitOfWork.context.Chats
                    .Where(chatelem => chatelem.Contact.MessengerUser.Id == chatDTO.ConcreteUserId)
                    .Where(chatelem => chatelem.ChatId == chatDTO.ChatId)
                    .Select(chatelem => chatelem.Contact.MessengerUserContact.UserProfile.Name)
                    .SingleOrDefault());
            }

            var LastMassageText = await Task.Run(() => unitOfWork.context.Messages
                .Where(m => m.Chat.ChatId == chatDTO.ChatId)
                .Select(m => m.Text)
                .FirstOrDefault());

            var LastMassageDate = await Task.Run(() => unitOfWork.context.Messages
                .Where(m => m.Chat.ChatId == chatDTO.ChatId)
                .Select(m => m.Date.ToString().Substring(0, 11))
                .FirstOrDefault());

            return (new ChatDTO
            {
                ContactName = ContactName,
                ChatId = chatDTO.ChatId,
                LastMassageText = LastMassageText,
                LastMassageDate = LastMassageDate,
            });
        }

        /// <summary>
        /// Send message to chat.
        /// </summary>
        /// <param name="messageDTO">MessageDTO to send</param>
        public async Task SendMessageToChat(MessageDTO messageDTO)
        {
            if (messageDTO == null)
                throw new ArgumentNullException("messageDTO");

            await Task.Run(() => unitOfWork.GetRepository<Message>().Create(new Message
            {
                Text = messageDTO.Text,
                Date = Convert.ToDateTime(messageDTO.Date),
                MassageSenderId = messageDTO.MassageSenderId,
                Chat = unitOfWork.context.Chats.Find(messageDTO.ChatId)
            }));

            await unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// Refuse chat deleting(soft delete).
        /// </summary>
        /// <param name="chatId">Chat id to reestablish.</param>
        public async Task ReestablishChat(int chatId)
        {
            var ChatToUpdate = await Task.Run(() => unitOfWork.GetRepository<Chat>().GetAsync(chatId));
            ChatToUpdate.DeletedState = false;
            await unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// Create message complaint.
        /// </summary>
        /// <param name="messageComplaintDTO">messageComplaintDTO.</param>
        public async Task CreateMessageComplaint(MessageComplaintDTO messageComplaintDTO)
        {
            var Message = await unitOfWork.GetRepository<Message>().GetAsync(messageComplaintDTO.MessageId);

            await Task.Run(() => unitOfWork.GetRepository<MessageComplaint>().Create(new MessageComplaint { 
                ComplaintText = messageComplaintDTO.ComplaintText,
                Message = Message,
                AccuserUserId = messageComplaintDTO.AccuserUserId

            }));

            await unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// Disposes unit of work.
        /// </summary>
        public void Dispose()
        {
            unitOfWork.Dispose();
        }
    }
}
