﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    /// <summary>
    /// User service implementation.
    /// </summary>
    public class UserService : IUserService
    {
        IUnitOfWork<MessengerContext> unitOfWork { get; set; }
        IContactService contactService { get; set; }

        /// <summary>
        /// Contructor initialize UnitOfWork and contact service.
        /// </summary>
        /// <param name="unitOfWorkInput">Library context unit of work object.</param>
        public UserService(IUnitOfWork<MessengerContext> unitOfWorkInput)
        {
            unitOfWork = unitOfWorkInput;
            contactService = new ContactService(unitOfWorkInput);
        }

        /// <summary>
        /// Get user by id.
        /// </summary>
        /// <param name="UserId">Id of user to get.</param>
        /// <returns>UserDTO.</returns>
        public async Task<UserDTO> GetUserById(string UserId)
        {
            MessengerUser messengerUser = await unitOfWork.UserManager.FindByIdAsync(UserId);
            if (messengerUser == null)
                return null;

            var config = new MapperConfiguration(cfg => cfg.CreateMap<UserProfile, UserDTO>()
               .ForMember(dto => dto.Birthday, opt => opt.MapFrom(profile => profile.Birthday))
               .ForMember(dto => dto.Email, opt => opt.MapFrom(profile => profile.MessengerUser.Email))
               .ForMember(dto => dto.Status, opt => opt.MapFrom(profile => profile.Status))
               .ForMember(dto => dto.PhoneNumber, opt => opt.MapFrom(profile => profile.PhoneNumber))
               .ForMember(dto => dto.UserId, opt => opt.MapFrom(profile => profile.MessengerUser.Id)));

            var mapper = new Mapper(config);
            return mapper.Map<UserDTO>(messengerUser.UserProfile);
        }

        /// <summary>
        /// Create new user in system.
        /// </summary>
        /// <param name="userDto">UserDTO with inforamtion about a new user.</param>
        /// <returns>Operation details.</returns>
        public async Task<OperationDetails> Create(UserDTO userDto)
        {
            MessengerUser user = await unitOfWork.UserManager.FindByEmailAsync(userDto.Email);
            if (user == null)
            {
                user = new MessengerUser { Email = userDto.Email, UserName = userDto.Email };
                var result = await unitOfWork.UserManager.CreateAsync(user, userDto.Password);
                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
                await unitOfWork.UserManager.AddToRoleAsync(user.Id, userDto.Role);
                UserProfile clientProfile = new UserProfile { 
                    UserProfileId = user.Id, 
                    Name = userDto.Name, 
                    Birthday = userDto.Birthday,
                    PhoneNumber = userDto.PhoneNumber
                };
                unitOfWork.GetRepository<UserProfile>().Create(clientProfile);

                await unitOfWork.SaveChangesAsync();
                return new OperationDetails(true, "Registration successfully!", "");
            }
            else
            {
                return new OperationDetails(false, "User with this login alredy exist!", "Email");
            }
        }

        /// <summary>
        /// Authentication method.
        /// </summary>
        /// <param name="userDto">UserDTO.</param>
        /// <returns>Claim.</returns>
        public async Task<ClaimsIdentity> Authenticate(UserDTO userDto)
        {
            ClaimsIdentity claim = null;
            MessengerUser user = await unitOfWork.UserManager.FindAsync(userDto.Email, userDto.Password);
            if (user != null)
                claim = await unitOfWork.UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            return claim;
        }

        /// <summary>
        /// Get all users in system.
        /// </summary>
        /// <returns>List of UserDTOs.</returns>
        public async Task<IEnumerable<UserDTO>> GetAll()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<UserProfile, UserDTO>()
              .ForMember(dto => dto.Birthday, opt => opt.MapFrom(profile => profile.Birthday))
              .ForMember(dto => dto.Email, opt => opt.MapFrom(profile => profile.MessengerUser.Email))
              .ForMember(dto => dto.Status, opt => opt.MapFrom(profile => profile.Status))
              .ForMember(dto => dto.PhoneNumber, opt => opt.MapFrom(profile => profile.PhoneNumber))
              .ForMember(dto => dto.UserId, opt => opt.MapFrom(profile => profile.MessengerUser.Id)));

            var mapper = new Mapper(config);
            return mapper.Map<List<UserDTO>>(await unitOfWork.GetRepository<UserProfile>().GetAllAsync());
        }

        /// <summary>
        /// Get all users in system without current.
        /// </summary>
        /// <param name="curentUserId">Current user id that will not be in the result list.</param>
        /// <returns>List of UserDTOs.</returns>
        public async Task<IEnumerable<UserDTO>> GetAllWithoutCurrent(string curentUserId)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<UserProfile, UserDTO>()
              .ForMember(dto => dto.Birthday, opt => opt.MapFrom(profile => profile.Birthday))
              .ForMember(dto => dto.Email, opt => opt.MapFrom(profile => profile.MessengerUser.Email))
              .ForMember(dto => dto.Status, opt => opt.MapFrom(profile => profile.Status))
              .ForMember(dto => dto.PhoneNumber, opt => opt.MapFrom(profile => profile.PhoneNumber))
              .ForMember(dto => dto.BanState, opt => opt.MapFrom(profile => profile.MessengerUser.BanState))
              .ForMember(dto => dto.UserId, opt => opt.MapFrom(profile => profile.MessengerUser.Id)));
                
            var mapper = new Mapper(config);
            return mapper.Map<List<UserDTO>>(await Task.Run(() => 
                unitOfWork.context.UserProfiles.Where(u =>u.MessengerUser.Id != curentUserId)));
        }

        /// <summary>
        /// Private synchronous method Determines is the user role is admin.
        /// </summary>
        /// <param name="userId">userId to analyze.</param>
        /// <remarks>
        /// True - user is admin.
        /// False - user is not admin.
        /// </remarks>
        /// <returns>Bollean result.</returns>
        private bool IsUserAdminSync(string userId)
        {
            return unitOfWork.UserManager.IsInRole(userId, RoleTypes.Admin.ToString());
        }

        /// <summary>
        /// Find(search) user by userDTO.
        /// </summary>
        /// <param name="userDTOSearchTerms">userDTOSearchTerms with terms to search.</param>
        /// <returns>List of finded users.</returns>
        public async Task<IEnumerable<UserDTO>> FindUsers(UserDTO userDTOSearchTerms)
        {
            IEnumerable<UserDTO> FoundUsers = await GetAll();
            await Task.Run(() => FoundUsers = FoundUsers.Where(user => IsUserAdminSync(user.UserId) == false));
            IEnumerable<ContactDTO> CurrentUserContacts = await contactService.GetContactsByUserIdAsync(userDTOSearchTerms.UserId);

            if (userDTOSearchTerms.Name != null)
            {
                await Task.Run(() => FoundUsers = FoundUsers.Where(user => user.Name.ToLower().Contains(userDTOSearchTerms.Name.ToLower())));
            }
            else if (userDTOSearchTerms.Email != null)
            {
                await Task.Run(() => FoundUsers = FoundUsers.Where(user => user.Email.ToLower().Contains(userDTOSearchTerms.Email.ToLower())));
            }
            else if (userDTOSearchTerms.PhoneNumber != null)
            {
                await Task.Run(() => FoundUsers = FoundUsers.Where(user => user.PhoneNumber.Contains(userDTOSearchTerms.PhoneNumber)));
            }
            return await Task.Run(() => FoundUsers
                .Where(user => user.UserId != userDTOSearchTerms.UserId)
                .Where(user => !CurrentUserContacts.Any(contact => contact.ContactUserId == user.UserId))); 
        }

        /// <summary>
        /// Update user profile state.
        /// </summary>
        /// <param name="userDTO">UserDTO with new user profile information.</param>
        public async Task UpdateUserProfile(UserDTO userDTO)
        {
            UserProfile oldProfile = await Task.Run(() => unitOfWork.context.UserProfiles
                .Where(up => up.MessengerUser.Id == userDTO.UserId)
                .SingleOrDefault());

            await Task.Run(() => unitOfWork.context.UserProfiles
                .Where(up => up.UserProfileId == oldProfile.UserProfileId)
                .SingleOrDefault().Name = userDTO.Name
            );

            await Task.Run(() => unitOfWork.context.UserProfiles
               .Where(up => up.UserProfileId == oldProfile.UserProfileId)
               .SingleOrDefault().Birthday = userDTO.Birthday
            );

            await Task.Run(() => unitOfWork.context.UserProfiles
               .Where(up => up.UserProfileId == oldProfile.UserProfileId)
               .SingleOrDefault().Status = userDTO.Status
            );

            await Task.Run(() => unitOfWork.context.UserProfiles
              .Where(up => up.UserProfileId == oldProfile.UserProfileId)
              .SingleOrDefault().PhoneNumber = userDTO.PhoneNumber
           );

            await unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// Determines is the user exists.
        /// </summary>
        /// <param name="userId">userId to find.</param>
        /// <remarks>
        /// True - exists.
        /// False - not exists.
        /// </remarks>
        /// <returns>Bollean result.</returns>
        public async Task<bool> IsUserExists(string userId)
        {
            MessengerUser messengerUser = await unitOfWork.UserManager.FindByIdAsync(userId);
            if (messengerUser == null)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Determines is the user role is admin.
        /// </summary>
        /// <param name="userId">userId to analyze.</param>
        /// <remarks>
        /// True - user is admin.
        /// False - user is not admin.
        /// </remarks>
        /// <returns>Bollean result.</returns>
        public async Task<bool> IsUserAdmin(string userId)
        {
            return await unitOfWork.UserManager.IsInRoleAsync(userId, RoleTypes.Admin.ToString());
        }

        /// <summary>
        /// Determines is the user baned by userDto.
        /// </summary>
        /// <param name="userId">UserDTO to analyze.</param>
        /// <remarks>
        /// True - user is banned.
        /// False - user is not banned.
        /// </remarks>
        /// <returns>Bollean result.</returns>
        public async Task<bool> IsUserBanned(UserDTO userDTO)
        {
            MessengerUser messengerUser = await unitOfWork.UserManager.FindAsync(userDTO.Email, userDTO.Password);
            return messengerUser.BanState;
        }

        /// <summary>
        /// Determines is the user baned by user id.
        /// </summary>
        /// <param name="userId">User id to analyze.</param>
        /// <remarks>
        /// True - user is banned.
        /// False - user is not banned.
        /// </remarks>
        /// <returns>Bollean result.</returns>
        public async Task<bool> IsUserBanned(string userId)
        {
            MessengerUser messengerUser = await unitOfWork.UserManager.FindByIdAsync(userId);
            return messengerUser.BanState;
        }

        /// <summary>
        /// Disposes unit of work and contact service.
        /// </summary>
        public void Dispose()
        {
            contactService.Dispose();
            unitOfWork.Dispose();
        } 
    }
}
