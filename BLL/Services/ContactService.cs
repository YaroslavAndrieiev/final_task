﻿using BLL.DTO;
using BLL.Interfaces;
using DAL.EF;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using AutoMapper;
using DAL.Entities;

namespace BLL.Services
{
    /// <summary>
    /// Contact service implementation.
    /// </summary>
    public class ContactService : IContactService
    {
        IUnitOfWork<MessengerContext> unitOfWork { get; set; }
        IChatService chatService { get; set; }

        /// <summary>
        /// Contructor initialize UnitOfWork and ChatService.
        /// </summary>
        /// <param name="unitOfWorkInput">Library context unit of work object.</param>
        public ContactService(IUnitOfWork<MessengerContext> unitOfWorkInput)
        {
            unitOfWork = unitOfWorkInput;
            chatService = new ChatService(unitOfWorkInput);
        }

        /// <summary>
        /// Get user contacnts.
        /// </summary>
        /// <param name="userId">User id to get contacts.</param>
        /// <returns>List of ContactDTOs/</returns>
        public async Task<IEnumerable<ContactDTO>> GetContactsByUserIdAsync(string userId)
        {
            List<ContactDTO> contactDTOs = new List<ContactDTO>();
            var contacts = await Task.Run(() => unitOfWork.context.Contacts
                .Where(c => c.MessengerUser.Id == userId)
                .Where(c => c.DeletedState == false)
            );

            foreach (var contact in contacts)
            {
                var ContactUserName = await Task.Run(() => unitOfWork.context.Contacts
                    .Where(c => c.ContactId == contact.ContactId)
                    .Select(c => c.MessengerUserContact.UserProfile.Name)
                    .FirstOrDefault()
                );
                
                var ContactUserId = await Task.Run(() => unitOfWork.context.Contacts
                    .Where(c => c.ContactId == contact.ContactId)
                    .Select(c => c.MessengerUserContact.Id)
                    .FirstOrDefault()
                );

                contactDTOs.Add(new ContactDTO {
                    ContactId = contact.ContactId,
                    ContactUserName = ContactUserName,
                    ContactUserId = ContactUserId,
                });
            }

            return contactDTOs;
        }

        /// <summary>
        /// Delete contact by id(soft delete).
        /// </summary>
        /// <param name="contactId">Contact id to delete.</param>
        public async Task DeleteContactAsync(int contactId)
        {
            var contactToDelete = await Task.Run(() => unitOfWork.context.Contacts
                 .Where(contact => contact.ContactId == contactId)
                 .FirstOrDefault()
             );
            contactToDelete.DeletedState = true;
            await unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// Create contact benwee to users.
        /// </summary>
        /// <param name="newContactDTO">newContactDTO.</param>
        public async Task AddContactAsync(ContactDTO newContactDTO)
        {
            var deletedContact = await Task.Run(() => unitOfWork.context.Contacts
                .Where(contact => contact.DeletedState == true)
                .Where(Contact => Contact.MessengerUser.Id == newContactDTO.UserId)
                .Where(Contact => Contact.MessengerUserContact.Id == newContactDTO.ContactUserId)
                .SingleOrDefault());

            if (deletedContact != null)
                deletedContact.DeletedState = false;
            else
            {
                var NewContact = new Contact
                {
                    MessengerUser = await unitOfWork.UserManager.FindByIdAsync(newContactDTO.UserId),
                    MessengerUserContact = await unitOfWork.UserManager.FindByIdAsync(newContactDTO.ContactUserId)
                };
                await Task.Run(() => unitOfWork.GetRepository<Contact>().Create(NewContact));
                await chatService.CreateChatByContactId(NewContact.ContactId);
            }
            await unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// Disposes unit of work and chat service.
        /// </summary>
        public void Dispose()
        {
            chatService.Dispose();
            unitOfWork.Dispose();
        }
    }
}
