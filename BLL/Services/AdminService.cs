﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BLL.Services
{
    /// <summary>
    /// Admin service implementation.
    /// </summary>
    public class AdminService : IAdminService
    {
        IUnitOfWork<MessengerContext> unitOfWork { get; set; }
        IUserService userService { get; set; }

        /// <summary>
        /// Contructor initialize UnitOfWork and user service.
        /// </summary>
        /// <param name="unitOfWorkInput">Library context unit of work object.</param>
        public AdminService(IUnitOfWork<MessengerContext> unitOfWorkInput)
        {
            unitOfWork = unitOfWorkInput;
            userService = new UserService(unitOfWorkInput);
        }

        /// <summary>
        /// Ban user. Set BanState field true.
        /// </summary>
        /// <param name="userId">Id of user to ban.</param>
        public async Task BanUser(string userId)
        {
            var userToBan = await unitOfWork.UserManager.FindByIdAsync(userId);
            userToBan.BanState = true;
            await unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// Refuses user ban. Set BanState field false.
        /// </summary>
        /// <param name="userId">Id of user to ban.</param>
        public async Task UnBanUser(string userId)
        {
            var userToBan = await unitOfWork.UserManager.FindByIdAsync(userId);
            userToBan.BanState = false;
            await unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes message complaint (soft delete). Set DeletedState true.
        /// </summary>
        /// <param name="complaintId">Message complaint id to delete.</param>
        public async Task DeleteComplaint(int complaintId)
        {
            var complaintToDelete = await Task.Run(() => unitOfWork.GetRepository<MessageComplaint>().GetAsync(complaintId));
            complaintToDelete.DeletedState = true;
            await unitOfWork.SaveChangesAsync();
        }

        /// <summary>
        /// Get all messages complaints.
        /// </summary>
        /// <returns>List of MessageComplaintDTO.</returns>
        public async Task<IEnumerable<MessageComplaintDTO>> GetMessageComplaints()
        {
            List<MessageComplaintDTO> messageComplaints = new List<MessageComplaintDTO>();

            foreach(var complaint in unitOfWork.context.MessageComplaints.Where(c => c.DeletedState == false))
            {
                var SenderUserId = await Task.Run(() => unitOfWork.context.MessageComplaints
                    .Where(c => c.MessageComplaintId == complaint.MessageComplaintId)
                    .Select(c => c.Message.MassageSenderId)
                    .SingleOrDefault()
                );
                var UserDto = await userService.GetUserById(SenderUserId);

                messageComplaints.Add(new MessageComplaintDTO
                {
                    MessageComplaintId = await Task.Run(() => unitOfWork.context.MessageComplaints
                        .Where(c => c.MessageComplaintId == complaint.MessageComplaintId)
                        .Select(c => c.MessageComplaintId)
                        .SingleOrDefault()
                    ),
                    MessageId = await Task.Run(() => unitOfWork.context.MessageComplaints
                        .Where(c => c.MessageComplaintId == complaint.MessageComplaintId)
                        .Select(c => c.Message.MessageId)
                        .SingleOrDefault()
                    ),
                    AccuserUserId = await Task.Run(() => unitOfWork.context.MessageComplaints
                        .Where(c => c.MessageComplaintId == complaint.MessageComplaintId)
                        .Select(c => c.AccuserUserId)
                        .SingleOrDefault()
                    ),
                    ComplaintText = await Task.Run(() => unitOfWork.context.MessageComplaints
                        .Where(c => c.MessageComplaintId == complaint.MessageComplaintId)
                        .Select(c => c.ComplaintText)
                        .SingleOrDefault()
                    ),
                    MessageText = await Task.Run(() => unitOfWork.context.MessageComplaints
                        .Where(c => c.MessageComplaintId == complaint.MessageComplaintId)
                        .Select(c => c.Message.Text)
                        .SingleOrDefault()
                    ),
                    SenderUserId = SenderUserId,
                    SenderName = UserDto.Name,
                });
            }

            return messageComplaints;
        }

        /// <summary>
        /// Disposes unit of work and user service.
        /// </summary>
        public void Dispose()
        {
            userService.Dispose();
            unitOfWork.Dispose();
        }
    }
}
