﻿using DAL.Interfaces;
using DAL.Repositories;
using DAL.EF;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Infrastructure
{
    /// <summary>
    /// BLL NinjectModule. Register BLL dependesies.
    /// </summary>
    public class BLLNinjectRegistration : NinjectModule
    {
        private string connectionString;

        public BLLNinjectRegistration(string connection)
        {
            connectionString = connection;
        }

        public override void Load()
        {
            Bind<IUnitOfWork<MessengerContext>>().To<UnitOfWork>().WithConstructorArgument(connectionString);
        }
    }
}
