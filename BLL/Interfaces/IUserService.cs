﻿using BLL.DTO;
using BLL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    /// <summary>
    /// User service interface.
    /// </summary>
    public interface IUserService : IDisposable
    {
        Task<OperationDetails> Create(UserDTO userDto);
        Task<ClaimsIdentity> Authenticate(UserDTO userDto);
        Task<UserDTO> GetUserById(string UserId);
        Task<IEnumerable<UserDTO>> GetAll();
        Task<IEnumerable<UserDTO>> GetAllWithoutCurrent(string curentUserId);
        Task UpdateUserProfile(UserDTO userDTO);
        Task<bool> IsUserExists(string userId);
        Task<bool> IsUserAdmin(string userId);
        Task<bool> IsUserBanned(UserDTO userDTO);
        Task<bool> IsUserBanned(string userId);
        Task<IEnumerable<UserDTO>> FindUsers(UserDTO userDTOSearchTerms);
    }
}
