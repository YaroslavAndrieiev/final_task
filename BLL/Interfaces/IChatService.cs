﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    /// <summary>
    /// Chat service interface.
    /// </summary>
    public interface IChatService
    {
        Task<IEnumerable<MessageDTO>> GetMessagesByChatIdAsync(int chatId);
        Task<IEnumerable<ChatDTO>> GetChatsAsync(string userId);
        Task<IEnumerable<ChatDTO>> GetAvailableForAddingChatsAsync(string userId);
        Task<ChatDTO> GetChat(ChatDTO chatDTO);
        Task CreateChatByContactId(int contactId);
        Task SendMessageToChat(MessageDTO messageDTO);
        Task DeleteChatById(int chatId);
        Task ReestablishChat(int chatId);
        Task CreateMessageComplaint(MessageComplaintDTO messageComplaintDTO);
        void Dispose();
    }
}
