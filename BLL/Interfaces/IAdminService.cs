﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace BLL.Interfaces
{
    /// <summary>
    /// Admin service interface.
    /// </summary>
    public interface IAdminService
    {
        Task<IEnumerable<MessageComplaintDTO>> GetMessageComplaints();
        Task BanUser(string userId);
        Task UnBanUser(string userId);
        Task DeleteComplaint(int complaintId);
        void Dispose();
    }
}
