﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    /// <summary>
    /// Contact service interface.
    /// </summary>
    public interface IContactService
    {
        Task<IEnumerable<ContactDTO>> GetContactsByUserIdAsync(string userId);
        Task DeleteContactAsync(int contactId);
        Task AddContactAsync(ContactDTO newContactDTO);
        void Dispose();
    }
}
