﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class MessageComplaintDTO
    {
        public int MessageComplaintId { get; set; }
        public int MessageId { get; set; }
        public string SenderUserId { get; set; }
        public string AccuserUserId { get; set; }
        public string SenderName { get; set; }
        public string ComplaintText { get; set; }
        public string MessageText { get; set; }
    }
}
