﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class ChatDTO
    {
        public int ChatId { get; set; }
        public string ConcreteUserId { get; set; }
        public string ContactName { get; set; }
        public string LastMassageText { get; set; }
        public string LastMassageDate { get; set; }
        public bool DeletedState { get; set; }
    }
}
