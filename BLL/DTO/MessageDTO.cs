﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class MessageDTO
    {
        public int ChatId { get; set; }
        public int MessageId { get; set; }
        public string MassageSenderId { get; set; }
        public string Text { get; set; }
        public string Date { get; set; }
    }
}
