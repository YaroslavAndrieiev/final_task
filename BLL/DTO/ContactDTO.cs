﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class ContactDTO
    {
        public int ContactId { get; set; }
        public string UserId { get; set; }
        public string ContactUserId { get; set; }
        public string ContactUserName { get; set; }
    }
}
