﻿using BLL.Interfaces;
using BLL.Services;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.Util
{
    /// <summary>
    /// NinjectModule register Services.
    /// </summary>
    public class UILNinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<IChatService>().To<ChatService>();
            Bind<IUserService>().To<UserService>();
            Bind<IContactService>().To<ContactService>();
            Bind<IAdminService>().To<AdminService>();
        }
    }
}