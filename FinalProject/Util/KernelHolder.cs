﻿using BLL.Infrastructure;
using BLL.Interfaces;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.Util
{
    /// <summary>
    /// Kernel holder creates ninject modules, and user service.
    /// </summary>
    public static class KernelHolder
    {
        static StandardKernel kernel;

        public static StandardKernel Kernel
        {
            get
            {
                if (kernel == null)
                {
                    NinjectModule UILNinjectModule = new UILNinjectRegistrations();
                    NinjectModule BLLNinjectModule = new BLLNinjectRegistration("DBConnection");
                    kernel = new StandardKernel(BLLNinjectModule, UILNinjectModule);
                }
                return kernel;
            }
        }

        public static IUserService CreateUserService()
        {
            return KernelHolder.Kernel.Get<IUserService>();
        }
    }
}