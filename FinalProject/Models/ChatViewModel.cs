﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    /// <summary>
    /// Chat view model.
    /// </summary>
    public class ChatViewModel
    {
        public int ChatId { get; set; }
        public IEnumerable<MessageDTO> ChatMessages { get; set; }
        public string CurrentUserId { get; set; }
        public string FriendName { get; set; }

        [Required]
        [MaxLength(400, ErrorMessage = "Max length is 400!")]
        public string NewMessageText { get; set; }
    }
}