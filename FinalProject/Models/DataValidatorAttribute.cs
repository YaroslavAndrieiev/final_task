﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class DataValidatorAttribute : ValidationAttribute
    {
        public DataValidatorAttribute()
        {
        }

        public override bool IsValid(object value)
        {
            if (value == null)
                return true;
            else
            {
                var dt = (DateTime)value;
                DateTime min = new DateTime(1900, 01, 01);
                if (dt >= DateTime.Now || dt <= min)
                {
                    return false;
                }
                return true;
            }
        }
    }
}