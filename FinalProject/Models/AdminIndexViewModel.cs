﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class AdminIndexViewModel
    {
        public string UserName { get; set; }
        public IEnumerable<MessageComplaintDTO> MessageComplaints { get; set; }
        public IEnumerable<UserDTO> Users { get; set; }
    }
}