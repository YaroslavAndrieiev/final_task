﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class UserProfileViewModel
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "Please enter your Name")]
        [Display(Name = "Name:")]
        [RegularExpression(@"[a-zA-Z0-9А-Яа-я\s]+", ErrorMessage = "Characters are not allowed.")]
        [MaxLength(50, ErrorMessage = "Max length is 50!")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter your birthday")]
        [Display(Name = "Birthday:")]
        [DataType(DataType.Date)]
        [DataValidatorAttribute(ErrorMessage = "Impossible date!")]
        public DateTime Birthday { get; set; }

        [Display(Name = "Status:")]
        [RegularExpression(@"[a-zA-Z0-9-9А-Яа-я\s\.\,\?\-]+", ErrorMessage = "Characters are not allowed.")]
        [MaxLength(100, ErrorMessage = "Max length is 100!")]
        public string Status { get; set; }

        //[Required(ErrorMessage = "Please enter your email")]
        [EmailAddress(ErrorMessage = "Incorrect email")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Mail:")]
        [MaxLength(50, ErrorMessage = "Max length is 50!")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your phone number")]
        [Phone(ErrorMessage = "Incorrect phone")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone:")]
        [MaxLength(15, ErrorMessage = "Max length is 15!")]
        public string PhoneNumber { get; set; }

        public bool IsUpdateSuccess { get; set; }
    }
}