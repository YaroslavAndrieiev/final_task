﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Please enter your email")]
        [Display(Name = "Mail:")]
        [EmailAddress(ErrorMessage = "Incorrect email")]
        [DataType(DataType.EmailAddress)]
        [MaxLength(50, ErrorMessage = "Max length is 50!")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your password")]
        [DataType(DataType.Password)]
        [Display(Name = "Password:")]
        [MaxLength(50, ErrorMessage = "Max length is 50!")]
        public string Password { get; set; }
    }
}