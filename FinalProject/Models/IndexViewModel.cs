﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class IndexViewModel
    {
        public string UserName { get; set; }
        public UserProfileViewModel userProfileViewModel { get; set; }
        public IEnumerable<ChatDTO> Chats { get; set; }
    }
}