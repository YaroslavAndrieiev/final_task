﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class FriendsViewModel
    {
        public string UserName { get; set; }
        public UserProfileViewModel userProfileViewModel { get; set; }
        public IEnumerable<ContactDTO> Contacts { get; set; }
        public int ContactIdToDelete { get; set; }
    }
}