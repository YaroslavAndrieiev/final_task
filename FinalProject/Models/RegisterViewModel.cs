﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Please enter your email")]
        [Display(Name = "Mail:")]
        [EmailAddress(ErrorMessage = "Incorrect email")]
        [DataType(DataType.EmailAddress)]
        [MaxLength(50, ErrorMessage = "Max length is 50!")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your phone number")]
        [Phone(ErrorMessage = "Incorrect phone")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone:")]
        [MaxLength(15, ErrorMessage = "Max length is 15!")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Please enter your password")]
        [Display(Name = "Password:")]
        [DataType(DataType.Password)]
        [MaxLength(50, ErrorMessage = "Max length is 50!")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please confirm your password")]
        [Display(Name = "Confirm password:")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        [MaxLength(50, ErrorMessage = "Max length is 50!")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Please enter your name")]
        [Display(Name = "Name:")]
        [RegularExpression(@"[a-zA-Z0-9А-Яа-я\s]+", ErrorMessage = "Characters are not allowed.")]
        [MaxLength(50, ErrorMessage = "Max length is 50!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter your birthday")]
        [Display(Name = "Birthday:")]
        [DataType(DataType.Date)]
        [DataValidatorAttribute(ErrorMessage = "Impossible date!")]
        public DateTime Birthday { get; set; }
    }
}