﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace FinalProject.Models
{
    public class FriendsSearchViewModel
{
        [Display(Name = "Name:")]
        [RegularExpression(@"[a-zA-Z0-9А-Яа-я\s]+", ErrorMessage = "Characters are not allowed.")]
        [MaxLength(50, ErrorMessage = "Max length is 50!")]
        public string NameToSearch { get; set; }

        [Display(Name = "Mail:")]
        [EmailAddress(ErrorMessage = "Incorrect email")]
        [DataType(DataType.EmailAddress)]
        [MaxLength(50, ErrorMessage = "Max length is 50!")]
        public string MailToSearch { get; set; }

        [Display(Name = "Phone:")]
        [Phone(ErrorMessage = "Incorrect phone number")]
        [DataType(DataType.PhoneNumber)]
        [MaxLength(15, ErrorMessage = "Max length is 15!")]
        public string PhoneToSearch { get; set; }

        public IEnumerable<UserDTO> SearchedUsers { get; set; }
    }
}