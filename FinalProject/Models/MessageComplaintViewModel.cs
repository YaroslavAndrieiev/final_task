﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class MessageComplaintViewModel
    {
        public int MessageId { get; set;}

        [Required(ErrorMessage = "Please enter your complaint:")]
        [Display(Name = "Complaint text:")]
        [RegularExpression(@"[a-zA-Z0-9-9А-Яа-я\s\.\,\?\-]+", ErrorMessage = "Characters are not allowed.")]
        [MaxLength(100, ErrorMessage = "Max length is 100!")]
        public string ComplaintText { get; set; }

        public bool IsUpdateSuccess { get; set; }
    }
}