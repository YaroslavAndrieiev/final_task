﻿"use strict";

function OpenUserProfile() {
    document.getElementById("open").click();
}

function CloseUserProfile() {
    document.getElementById("userProfileCloseBtn").click();
}

function RefreshMain() {
    $('#user').click()
}

function OpenChat() {
    document.querySelectorAll('#chatsContainer div').forEach(e => e.addEventListener("click", chatClickHandler));
}
function chatClickHandler() {
    var chatsContainers = document.getElementById("chatsContainer").querySelectorAll("div");
    for (var i = 0; i < chatsContainers.length; i++) {
        if (chatsContainers[i].className == "horizontalContainerUserImageAndChat")
            chatsContainers[i].style.backgroundColor = "white";
    }

    if (this.className == "horizontalContainerUserImageAndChat") {
        if (this.style.backgroundColor != "rgb(2, 102, 69)") {
            this.style.backgroundColor = "rgb(2, 102, 69)";
            this.getElementsByTagName('a')[0].click();
        }
        else
            this.style.backgroundColor = "white";
    }
}   

function ScrollMessagesDown() {
    var scrollDiv = document.getElementById("ChatMessages");
    scrollDiv.scrollTop = scrollDiv.scrollHeight - scrollDiv.clientHeight;
    ClearMessageInput();
}

function DeleteClickHendler(context) {
    context.getElementsByTagName('input')[0].click();
}

function OpenFriendsSearch() {
    document.getElementById("openFriendsSearch").click();
    document.getElementById("showUsers").click();
}

function OpenChatsAdding() {
    document.getElementById("openChatsAdding").click();
    document.getElementById("showEvailableChats").click();
}

function OpenMessageComplaint(context) {
    while (context.id != "friendMessage") {
        context = context.parentElement;
    }
    var buttons = context.querySelectorAll("button");
    for (var i = 0; i < buttons.length; i++) {
        if (buttons[i].id == "openMessageComplaint")
            buttons[i].click();
    }
}

function CloseMessageComplaint() {
    document.getElementById("closeMessageComplaint").click();
}

function BanClickHandler(context) {
    context.parentElement.getElementsByTagName('input')[0].click();
}

function ClearMessageInput() {
    document.getElementById("messageInput").value = "";
}
