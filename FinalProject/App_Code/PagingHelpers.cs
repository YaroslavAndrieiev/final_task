﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FinalProject.App_Code
{
    public static class PagingHelpers
    {
        /// <summary>
        /// Custom halper create dropdown menu with sing out and profile options.
        /// </summary>
        /// <param name="UserName">Name of current user.</param>
        /// <returns>Halper</returns>
        public static MvcHtmlString ShowUserOptions(this HtmlHelper html, string UserName)
        {
            StringBuilder result = new StringBuilder();
            result.Append("<div class='dropdownUserOptions'>");
            result.Append("<button class='dropbtn' id='user'>" + UserName + "</button>");
            result.Append("<img alt=\"UserIcon\" src=\"/Content/Images/user.png\" style=\"width:40px;height:40px;\">");
            
            result.Append("<div class='dropdown-content'>");
            TagBuilder aLogOut = new TagBuilder("a");
            aLogOut.InnerHtml += "Sing out";
            aLogOut.MergeAttribute("href", "/Home/SingOut");
            result.Append(aLogOut.ToString());

            TagBuilder aProfile = new TagBuilder("a");
            aProfile.InnerHtml += "Profile";
            aProfile.MergeAttribute("style", "cursor:pointer;");
            aProfile.MergeAttribute("onClick", "return OpenUserProfile();");
            result.Append(aProfile.ToString());

            result.Append("</div>");
            result.Append("</div>");
            return MvcHtmlString.Create(result.ToString());
        }
    }
}