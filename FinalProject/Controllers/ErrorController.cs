﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FinalProject.Controllers
{
    /// <summary>
    /// Error conrtoller.
    /// </summary>
    public class ErrorController : Controller
    {
        /// <summary>
        /// Action will be called after the rising 404 error.
        /// </summary>
        /// <returns>PageNotFound view.</returns>
        public ActionResult PageNotFound()
        {
            return View();
        }

        /// <summary>
        /// Action will be called after the rising 500 error.
        /// </summary>
        /// <returns>ServerError view.</returns>
        public ActionResult ServerError()
        {
            return View();
        }
    }
}