﻿using BLL.DTO;
using BLL.Interfaces;
using FinalProject.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FinalProject.Controllers
{
    /// <summary>
    /// Friends controller.
    /// </summary>
    /// <remarks>
    /// Evailable only for user with role User.
    /// </remarks>
    [Authorize(Roles = "User")]
    public class FriendsController : Controller
    {
        IContactService contactService;

        /// <summary>
        /// Public constructor set contact service.
        /// </summary>
        /// <param name="service">Contact service.</param>
        public FriendsController(IContactService service)
        {
            contactService = service;
        }

        /// <summary>
        /// Get user service from HttpContext.
        /// </summary>
        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        /// <summary>
        /// Returns id of current autorized user.
        /// </summary>
        private string CurrentUserId
        {
            get
            {
                return User.Identity.GetUserId();
            }
        }

        /// <summary>
        /// Http get action show user fliends.
        /// </summary>
        /// <returns>View with user friends.</returns>
        [HttpGet]
        public async Task<ActionResult> GetUserFriends()
        {
            UserDTO currentUserDTO = await UserService.GetUserById(CurrentUserId);
            if (currentUserDTO == null)
                return RedirectToAction("SingOut", "Home");

            FriendsViewModel friendsViewModel = new FriendsViewModel();
            currentUserDTO.UserId = User.Identity.GetUserId();
            friendsViewModel.UserName = currentUserDTO.Name;

            friendsViewModel.userProfileViewModel = new UserProfileViewModel
            {
                UserName = currentUserDTO.Name,
                Birthday = currentUserDTO.Birthday,
                Status = currentUserDTO.Status,
                Email = currentUserDTO.Email,
                UserId = currentUserDTO.UserId,
                PhoneNumber = currentUserDTO.PhoneNumber
            };

            friendsViewModel.Contacts = await contactService.GetContactsByUserIdAsync(currentUserDTO.UserId);

            return View(friendsViewModel);
        }

        /// <summary>
        /// Http post action add friend by user id.
        /// </summary>
        /// <param name="UserId">Friend id to add.</param>
        /// <returns>Redirect to action GetUserFriends.</returns>
        [HttpPost]
        public async Task<ActionResult> AddFriend(string UserId)
        {
            await contactService.AddContactAsync(new ContactDTO {
                UserId = CurrentUserId,
                ContactUserId = UserId
            });

            return RedirectToAction("GetUserFriends");
        }

        /// <summary>
        /// Find users by entered searching terms.
        /// </summary>
        /// <param name="friendsSearchViewModel">View model with terms to search.</param>
        /// <returns>Partial view FriendsSearchPartial.</returns>
        [HttpGet]
        public async Task<ActionResult> FindUsers(FriendsSearchViewModel friendsSearchViewModel)
        {
            friendsSearchViewModel.SearchedUsers = await UserService.FindUsers(new UserDTO
            {
                Name = friendsSearchViewModel.NameToSearch,
                Email = friendsSearchViewModel.MailToSearch,
                PhoneNumber = friendsSearchViewModel.PhoneToSearch,
                UserId = CurrentUserId
            });

            return PartialView("FriendsSearchPartial", friendsSearchViewModel);
        }

        /// <summary>
        /// Http post action delete friend by contact id.
        /// </summary>
        /// <param name="ContactId">Contact id to delete.</param>
        /// <returns>Redirect to action GetUserFriends.</returns>
        [HttpPost]
        public async Task<ActionResult> DeleteFriend(int ContactId)
        {
            await contactService.DeleteContactAsync(ContactId);
            return RedirectToAction("GetUserFriends");
        }

        /// <summary>
        /// Disposes services.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            UserService.Dispose();
            contactService.Dispose();
            base.Dispose(disposing);
        }
    }
}