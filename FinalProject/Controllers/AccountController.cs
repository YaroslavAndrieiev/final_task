﻿using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using FinalProject.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace FinalProject.Controllers
{
    public class AccountController : Controller
    {
        /// <summary>
        /// Get user service from HttpContext.
        /// </summary>
        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        /// <summary>
        /// Get AuthenticationManager from HttpContext.
        /// </summary>
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        /// <summary>
        /// Http get login action.
        /// </summary>
        /// <returns>
        /// Redirect to Home/Index if the user isn't an admin and not banned.
        /// Redirect to Admin/Index if the user is admin.
        /// </returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> Login()
        {
            if (User.Identity.IsAuthenticated == true)
            {
                var UserExists = await UserService.IsUserExists(User.Identity.GetUserId());
                if (UserExists == true)
                {
                    if (await UserService.IsUserBanned(User.Identity.GetUserId()) == true)
                    {
                        AuthenticationManager.SignOut();
                        return RedirectToAction("Login");
                    }
                    else
                    {
                        if (await UserService.IsUserAdmin(User.Identity.GetUserId()) == true)
                            return RedirectToAction("Index", "Admin");

                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            return View();
        }

        /// <summary>
        /// Reguster action.
        /// </summary>
        /// <remarks>Available for unregistered users.</remarks>
        /// <returns>Register view.</returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Registration()
        {
            return View();
        }

        /// <summary>
        /// Login post action takes LoginViewModel validate it if the data is valid sing in the system.
        /// </summary>
        /// <param name="model">LoginViewModel with entered data.</param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO { Email = model.Email, Password = model.Password };
                ClaimsIdentity claim = await UserService.Authenticate(userDto);

                if (claim == null)

                {
                    ModelState.AddModelError("", "Incorrect login or password.");
                }
                else if (await UserService.IsUserBanned(userDto) == true)
                {
                    ModelState.AddModelError("", "This accout is banned! Please contact support workers.");
                }
                else
                {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);

                    return RedirectToAction("Login");
                }
            }
            return View(model);
        }

        /// <summary>
        /// Logout and redirect to login.
        /// </summary>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }


        /// <summary>
        /// Register post action takes RegisterViewModel validate it if the data is valid creates a new user.
        /// </summary>
        /// <param name="model">RegisterViewModel with entered data.</param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> Registration(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO
                {
                    Email = model.Email,
                    Password = model.Password,
                    Name = model.Name,
                    Birthday = model.Birthday,
                    Role = "user",
                    PhoneNumber = model.PhoneNumber
                };
                OperationDetails operationDetails = await UserService.Create(userDto);
                if (operationDetails.Succedeed)
                    return RedirectToAction("Login", "Account");
                else
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
            }
            return View(model);
        }

        /// <summary>
        /// Disposes services.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            UserService.Dispose();
            base.Dispose(disposing);
        }
    }
}