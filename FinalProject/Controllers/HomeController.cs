﻿using BLL.DTO;
using BLL.Interfaces;
using FinalProject.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FinalProject.Controllers
{
    /// <summary>
    /// Home contoller.
    /// </summary>
    /// <remarks>
    /// Evailable only for users with role User.
    /// </remarks>
    [Authorize(Roles = "User")]
    public class HomeController : Controller
    {
        IChatService chatService;

        /// <summary>
        /// Public constructor set chat service.
        /// </summary>
        /// <param name="service">Chat service.</param>
        public HomeController(IChatService service)
        {
            chatService = service;
        }

        /// <summary>
        /// Get user service from HttpContext.
        /// </summary>
        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        /// <summary>
        /// Returns id of current autorized user.
        /// </summary>
        private string CurrentUserId
        {
            get
            {
                return User.Identity.GetUserId();
            }
        }

        /// <summary>
        /// Shows main page with user chats.
        /// </summary>
        /// <returns>Index view with chats and current user profile information.</returns>
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            UserDTO currentUserDTO = await UserService.GetUserById(CurrentUserId);
            if (currentUserDTO == null)
                await SingOut();

            currentUserDTO.UserId = CurrentUserId;
            IndexViewModel indexViewModel = new IndexViewModel();
            indexViewModel.UserName = currentUserDTO.Name;
            IEnumerable<ChatDTO> chats = await chatService.GetChatsAsync(currentUserDTO.UserId);
            indexViewModel.Chats = chats;

            indexViewModel.userProfileViewModel = new UserProfileViewModel
            {
                UserName = currentUserDTO.Name,
                Birthday = currentUserDTO.Birthday,
                Status = currentUserDTO.Status,
                Email = currentUserDTO.Email,
                UserId = currentUserDTO.UserId,
                PhoneNumber = currentUserDTO.PhoneNumber
            };

            return View(indexViewModel);
        }

        /// <summary>
        /// Shows partial view with chat messages.
        /// </summary>
        /// <param name="ChatId">Chat id to get messages.</param>
        /// <returns>Partial view ChatMessages with messages.</returns>
        [HttpGet]
        public async Task<ActionResult> ChatMessages(int ChatId)
        {
            if (Request.IsAjaxRequest() == false)
                return RedirectToAction("Index");

            ChatDTO currentChatDTO = await chatService.GetChat(new ChatDTO { 
                ChatId = ChatId, 
                ConcreteUserId = CurrentUserId
            });

            ChatViewModel chatViewModel = new ChatViewModel {
                ChatMessages = await chatService.GetMessagesByChatIdAsync(ChatId),
                ChatId = ChatId,
                CurrentUserId = CurrentUserId,
                FriendName = currentChatDTO.ContactName
            };

            return PartialView(chatViewModel);
        }

        /// <summary>
        /// Shows evailable chats to adding.
        /// </summary>
        /// <param name="indexViewModel">indexViewModel.</param>
        /// <returns>Partial view ChatsAddingPartial.</returns>
        [HttpGet]
        public async Task<ActionResult> AvailableChats(IndexViewModel indexViewModel)
        {
            indexViewModel.Chats = await chatService.GetAvailableForAddingChatsAsync(CurrentUserId);
            return PartialView("ChatsAddingPartial", indexViewModel);
        }

        /// <summary>
        /// Post action refuse chat deleting.
        /// </summary>
        /// <param name="ChatId">Chat id to refuse.</param>
        /// <returns>Redirect to index action.</returns>
        [HttpPost]
        public async Task<ActionResult> ReestablishChat(int ChatId)
        {
            await chatService.ReestablishChat(ChatId);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Post action delete chat by id.
        /// </summary>
        /// <param name="ChatId">Chat id to delete.</param>
        /// <returns>Redirect to index action.</returns>
        [HttpPost]
        public async Task<ActionResult> DeleteChat(int ChatId)
        {
            await chatService.DeleteChatById(ChatId);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Post action send message to chat.
        /// </summary>
        /// <param name="chatViewModel">Chat view model with new message text and chat id.</param>
        /// <returns>Partial view ChatMessages.</returns>
        [HttpPost]
        public async Task<ActionResult> SendMessage(ChatViewModel chatViewModel)
        {
            ChatDTO currentChatDTO = await chatService.GetChat(new ChatDTO
            {
                ChatId = chatViewModel.ChatId,
                ConcreteUserId = CurrentUserId
            });

            if (ModelState.IsValid)
            {
                await chatService.SendMessageToChat(new MessageDTO
                {
                    ChatId = chatViewModel.ChatId,
                    Text = chatViewModel.NewMessageText,
                    Date = DateTime.Now.ToString(),
                    MassageSenderId = CurrentUserId,
                });

                chatViewModel.ChatMessages = await chatService.GetMessagesByChatIdAsync(chatViewModel.ChatId);
                chatViewModel.CurrentUserId = CurrentUserId;
                chatViewModel.FriendName = currentChatDTO.ContactName;
                return PartialView("ChatMessages", chatViewModel);
            }
            else
            {
                chatViewModel.ChatMessages = await chatService.GetMessagesByChatIdAsync(chatViewModel.ChatId);
                chatViewModel.CurrentUserId = CurrentUserId;
                chatViewModel.FriendName = currentChatDTO.ContactName;
                return PartialView("ChatMessages", chatViewModel);
            }
        }

        /// <summary>
        /// Post action send edit user profile information.
        /// </summary>
        /// <param name="userProfileViewModel">User profile view model with entered new user profile data.</param>
        /// <returns>Partial view UserProfilePartial.</returns>
        [HttpPost]
        public async Task<ActionResult> EditUserProfile(UserProfileViewModel userProfileViewModel)
        {
            if(ModelState.IsValid)
            {
                await UserService.UpdateUserProfile(new UserDTO
                {
                    UserId = CurrentUserId,
                    Birthday = userProfileViewModel.Birthday,
                    Status = userProfileViewModel.Status,
                    Name = userProfileViewModel.UserName,
                    PhoneNumber = userProfileViewModel.PhoneNumber,
                });
                userProfileViewModel.IsUpdateSuccess = true;
            }
           
            return PartialView("UserProfilePartial", userProfileViewModel);
        }

        /// <summary>
        /// Post action send message complaint.
        /// </summary>
        /// <param name="complaint">Message complaint view model with entered complaint text and message id.</param>
        /// <returns>Partial view MessageComplaintInputPartial.</returns>
        [HttpPost] 
        public async Task<ActionResult> SendMessageCompalint(MessageComplaintViewModel complaint)
        {
            if (ModelState.IsValid) 
            {
                await chatService.CreateMessageComplaint(new MessageComplaintDTO {
                    MessageId = complaint.MessageId,
                    ComplaintText  = complaint.ComplaintText,
                    AccuserUserId = CurrentUserId
                });

                complaint.IsUpdateSuccess = true;
                return PartialView("MessageComplaintInputPartial", complaint);
            }
            return PartialView("MessageComplaintInputPartial", complaint);
        }

        /// <summary>
        /// Sing out.
        /// </summary>
        /// <remarks>
        /// Evailable for any users from any parts of program.
        /// </remarks>
        /// <returns>Redirec to Account/Login action.</returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> SingOut()
        {
            var AuthenticationManager = HttpContext.GetOwinContext().Authentication;
            await Task.Run(() => AuthenticationManager.SignOut()); 
            return RedirectToAction("Login", "Account");
        }

        /// <summary>
        /// Disposes services.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            chatService.Dispose();
            UserService.Dispose();
            base.Dispose(disposing);
        }
    }
}