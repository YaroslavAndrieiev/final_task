﻿using BLL.DTO;
using BLL.Interfaces;
using FinalProject.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FinalProject.Controllers
{
    /// <summary>
    /// Admin controller.
    /// </summary>
    /// <remarks>
    /// Evailable for users with role Admin.
    /// </remarks>
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        IAdminService adminService;

        /// <summary>
        /// Set admin service.
        /// </summary>
        /// <param name="service">Admin service.</param>
        public AdminController(IAdminService service)
        {
            adminService = service;
        }

        /// <summary>
        /// Get user service from HttpContext.
        /// </summary>
        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        /// <summary>
        /// Returns id of current autorized user.
        /// </summary>
        private string CurrentUserId
        {
            get
            {
                return User.Identity.GetUserId();
            }
        }

        /// <summary>
        /// Http get action.
        /// </summary>
        /// <returns>View with message complaints.</returns>
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            UserDTO currentUserDTO = await UserService.GetUserById(CurrentUserId);
            if (currentUserDTO == null)
                return RedirectToAction("SingOut", "Home");

            AdminIndexViewModel adminIndexViewModel = new AdminIndexViewModel {
                UserName = currentUserDTO.Name,
                MessageComplaints = await adminService.GetMessageComplaints()
            };

            return View(adminIndexViewModel);
        }

        /// <summary>
        /// Http post action delete complaint by id.
        /// </summary>
        /// <param name="complaintId">Complaint id to delete.</param>
        /// <returns>Redirect to index action.</returns>
        [HttpPost]
        public async Task<ActionResult> DeleteComplaint(int complaintId)
        {
            await adminService.DeleteComplaint(complaintId);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Http post action ban user by id.
        /// </summary>
        /// <param name="userId">User id to ban.</param>
        /// <param name="redirectToIndex">Need of redirect to the index action.</param>
        /// <returns>
        /// If redirectToIndex is true redirect to index action.
        /// If redirectToIndex is false redirect to Users action.
        /// </returns>
        [HttpPost]
        public async Task<ActionResult> BanUser(string userId, bool redirectToIndex)
        {
            await adminService.BanUser(userId);
            if (redirectToIndex == true)
                return RedirectToAction("Index");
            return RedirectToAction("Users");
        }

        /// <summary>
        /// Http post action refuse ban by user id.
        /// </summary>
        /// <param name="userId">User id to unban.</param>
        /// <returns>Redirect to action Users.</returns>
        [HttpPost]
        public async Task<ActionResult> UnBanUser(string userId)
        {
            await adminService.UnBanUser(userId);
            return RedirectToAction("Users");
        }

        /// <summary>
        /// Http get action returns users view with all users in system.
        /// </summary>
        /// <returns>Users view.</returns>
        [HttpGet]
        public async Task<ActionResult> Users()
        {
            UserDTO currentUserDTO = await UserService.GetUserById(CurrentUserId);
            if (currentUserDTO == null)
                return RedirectToAction("SingOut", "Home");

            AdminIndexViewModel adminIndexViewModel = new AdminIndexViewModel
            {
                UserName = currentUserDTO.Name,
                Users = await UserService.GetAllWithoutCurrent(CurrentUserId)
            };
            return View(adminIndexViewModel);
        }

        /// <summary>
        /// Disposes services.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            UserService.Dispose();
            adminService.Dispose();
            base.Dispose(disposing);
        }
    }
}