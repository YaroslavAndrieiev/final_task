﻿using DAL.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    /// <summary>
    /// Generic unit of work interface.
    /// </summary>
    public interface IUnitOfWork<TContext> where TContext : DbContext
    {
        MessengerUserManager UserManager { get; }
        MessengerRoleManager RoleManager { get; }
        TContext context { get; }
        Task SaveChangesAsync();
        void SaveChanges();
        IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class;
        void Dispose();
    }
}
