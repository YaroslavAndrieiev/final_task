﻿using DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Text;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;

namespace DAL.EF
{
    /// <summary>
    /// Application data base Context.
    /// </summary>
    /// <remarks>
    /// Inherits from IdentityDbContext.
    /// </remarks>
    public class MessengerContext : IdentityDbContext<MessengerUser>
    {
        /// <summary>
        /// Static constructor set initializer to create some default data.
        /// </summary>
        static MessengerContext()
        {
            Database.SetInitializer<MessengerContext>(new MessengerContextInitializer());
        }
        /// <summary>
        /// Get connection string and send it to base constructor.
        /// </summary>
        /// <param name="connectionString">Database connection string.</param>
        public MessengerContext(string connectionString)
            : base(connectionString)
        {
        }

        /// <summary>
        /// Set entity framework settings and create some releations using fluent api.
        /// </summary>
        /// <param name="modelBuilder">Model builder.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Entity<UserProfile>()
                .HasRequired(p => p.MessengerUser)
                .WithOptional(c => c.UserProfile);

            modelBuilder.Entity<Chat>()
               .HasRequired(p => p.Contact)
               .WithOptional(c => c.Chat);
        }

        /// <summary>
        /// Overrided SaveChanges method displays more exception information.
        /// </summary>
        /// <returns>Base method save changes.</returns>
        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var errorMessages = ex.EntityValidationErrors
                .SelectMany(x => x.ValidationErrors)
                .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }
    
        /// <summary>
        /// User profiles db set.
        /// </summary>
        public DbSet<UserProfile> UserProfiles { get; set; }

        /// <summary>
        /// Chats db set.
        /// </summary>
        public DbSet<Chat> Chats { get; set; }

        /// <summary>
        /// Contacts db set.
        /// </summary>
        public DbSet<Contact> Contacts { get; set; }

        /// <summary>
        /// Messages db set.
        /// </summary>
        public DbSet<Message> Messages { get; set; }

        /// <summary>
        /// Messages complaints db set.
        /// </summary>
        public DbSet<MessageComplaint> MessageComplaints { get; set; }
    }
}
