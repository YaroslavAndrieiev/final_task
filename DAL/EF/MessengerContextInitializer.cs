﻿using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace DAL.EF
{
    /// <summary>
    /// Library context initializer.
    /// </summary>
    /// <remarks>
    ///<para>Initialize some default data and set it to the database.</para>
    ///<para>DropCreateDatabaseIfModelChanges - checks the database table definitions against models. If the models do not match the definition of tables, then the database is recreated.</para>
    /// </remarks>
    public class MessengerContextInitializer : DropCreateDatabaseIfModelChanges<MessengerContext>
    {
        /// <summary>
        /// Overrided method of creation data.
        /// </summary>
        /// <param name="db">Data context where default data will be added.</param>
        protected override void Seed(MessengerContext db)
        {
            var roleStore = new RoleStore<IdentityRole>(db);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            List<IdentityRole> identityRoles = new List<IdentityRole> {
                new IdentityRole () { Name = RoleTypes.Admin.ToString() },
                new IdentityRole() { Name = RoleTypes.User.ToString() }
            };
            foreach (IdentityRole role in identityRoles)
            {
                roleManager.Create(role);
            }
            var userStore = new UserStore<MessengerUser>(db);
            var userManager = new UserManager<MessengerUser>(userStore);

            var yarikUserProfile = new UserProfile {Name = "Yaroslav Andrieiev", Birthday = DateTime.Now, PhoneNumber = "+380981306665" };
            var yarik = new MessengerUser { Email = "yaroslav@gmail.com", UserName = "yaroslav@gmail.com", UserProfile = yarikUserProfile };
            userManager.Create(yarik, "YarikPswd");
            userManager.AddToRole(yarik.Id, RoleTypes.User.ToString());

            var adminUserProfile = new UserProfile { Name = "Admin Adminovich", Birthday = DateTime.Now, PhoneNumber = "+380989870001" };
            var admin = new MessengerUser { Email = "admin@gmail.com", UserName = "admin@gmail.com", UserProfile = adminUserProfile };
            userManager.Create(admin, "AdminPswd");
            userManager.AddToRole(admin.Id, RoleTypes.Admin.ToString());

            const string userPswdSample = "SamplePswd";
            var u2p = new UserProfile { Name = "Dmitriy", Birthday = DateTime.Now, PhoneNumber = "+380671230902" };
            var u2 = new MessengerUser { Email = "dima@gmail.com", UserName = "dima@gmail.com", UserProfile = u2p };
            userManager.Create(u2, userPswdSample);
            userManager.AddToRole(u2.Id, RoleTypes.User.ToString());

            var u3p = new UserProfile { Name = "Katya", Birthday = DateTime.Now, PhoneNumber = "+380675498212"};
            var u3 = new MessengerUser { Email = "katya@gmail.com", UserName = "katya@gmail.com", UserProfile = u3p };
            userManager.Create(u3, userPswdSample);
            userManager.AddToRole(u3.Id, RoleTypes.User.ToString());

            var u4p = new UserProfile { Name = "Evgeniy", Birthday = DateTime.Now, PhoneNumber = "+380662287771" };
            var u4 = new MessengerUser { Email = "evgeniy@gmail.com", UserName = "evgeniy@gmail.com", UserProfile = u4p };
            userManager.Create(u4, userPswdSample);
            userManager.AddToRole(u4.Id, RoleTypes.User.ToString());

            var u5p = new UserProfile { Name = "Kristina", Birthday = DateTime.Now, PhoneNumber = "+380985467192" };
            var u5 = new MessengerUser { Email = "kristina@gmail.com", UserName = "kristina@gmail.com", UserProfile = u5p };
            userManager.Create(u5, userPswdSample);
            userManager.AddToRole(u5.Id, RoleTypes.User.ToString());

            Contact c1 = new Contact { MessengerUser = yarik, MessengerUserContact = u2};
            Contact c2 = new Contact { MessengerUser = yarik, MessengerUserContact = u3};
            Contact c3 = new Contact { MessengerUser = u2, MessengerUserContact = yarik };
            Contact c4 = new Contact { MessengerUser = u3, MessengerUserContact = yarik };
            db.Contacts.AddRange(new List<Contact> 
            { 
                c1, c2, c3, c4
            });

            Chat ch1 = new Chat { Contact = c1 };
            Chat ch2 = new Chat { Contact = c2 };
            db.Chats.AddRange(new List<Chat>
            {
                ch1, ch2
            });

            Message m1 = new Message { Chat = ch1, Date = Convert.ToDateTime("19.11.2020"), MassageSenderId = yarik.Id, Text = "Hello, Dmitriy!"};
            Message m2 = new Message { Chat = ch2, Date = Convert.ToDateTime("19.11.2020"), MassageSenderId = yarik.Id, Text = "Hello, Katya!" };
            Message m3 = new Message { Chat = ch1, Date = Convert.ToDateTime("21.11.2020"), MassageSenderId = u2.Id, Text = "Hello, Yarik!" };
            Message m4 = new Message { Chat = ch2, Date = Convert.ToDateTime("21.11.2020"), MassageSenderId = u3.Id, Text = "Hello, Yarik!" };
            db.Messages.AddRange(new List<Message> 
            {
                m1, m2, m3, m4
            });
            db.SaveChanges();
        }  
    }
}
