﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    /// <summary>
    /// Chat data base entity.
    /// </summary>
    public class Chat
    {
        /// <summary>
        /// Primary key. Chat id.
        /// </summary>
        public int ChatId { get; set; }

        /// <summary>
        /// Deleted state. Provides soft delete.
        /// </summary>
        /// <remarks>
        /// True - chat deleted.
        /// False(default) - chat exists.
        /// </remarks>
        public bool DeletedState { get; set; }

        /// <summary>
        /// One-to-one relation with Contact entity.
        /// </summary>
        public virtual Contact Contact { get; set; }

        /// <summary>
        /// One-to-many relation with Message entity.
        /// </summary>
        public ICollection<Message> Messages { get; set; }

        /// <summary>
        /// Constructor set list of messages.
        /// </summary>
        public Chat()
        {
            Messages = new List<Message>();
        }
    }
}
