﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Entities
{
    /// <summary>
    /// Message complaint data base entity.
    /// </summary>
    public class MessageComplaint
    {
        /// <summary>
        /// Primary key. Message complaint id.
        /// </summary>
        public int MessageComplaintId { get; set; }

        /// <summary>
        /// Deleted state. Provides soft delete.
        /// </summary>
        /// <remarks>
        /// True - message complaint deleted.
        /// False(default) - message complaint exists.
        /// </remarks>
        public bool DeletedState { get; set; }

        /// <summary>
        /// Complaint text.
        /// </summary>
        public string ComplaintText { get; set; }

        /// <summary>
        /// One-to-many relation with Message entity.
        /// </summary>
        public Message Message { get; set; }

        /// <summary>
        /// Id of user that send a complaint.
        /// </summary>
        public string AccuserUserId { get; set; }
    }
}
