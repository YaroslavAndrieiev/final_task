﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Entities
{
    /// <summary>
    /// Contact data base entity.
    /// </summary>
    public class Contact
    {
        /// <summary>
        /// Primary key. Contact id.
        /// </summary>
        public int ContactId { get; set; }

        /// <summary>
        /// Deleted state. Provides soft delete.
        /// </summary>
        /// <remarks>
        /// True - contact deleted.
        /// False(default) - contact exists.
        /// </remarks>
        public bool DeletedState { get; set; }

        /// <summary>
        /// One-to-one relation with Chat entity.
        /// </summary>
        public virtual Chat Chat { get; set; }

        /// <summary>
        /// First One-to-many relation with MessengerUser entity.
        /// </summary>
        public MessengerUser MessengerUser { get; set; }

        /// <summary>
        /// Second One-to-many relation with MessengerUser entity.
        /// </summary>
        public MessengerUser MessengerUserContact { get; set; }
    }
}
