﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    /// <summary>
    /// Message data base entity.
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Primary key. Message id.
        /// </summary>
        public int MessageId { get; set; }

        /// <summary>
        /// Id of user that send message.
        /// </summary>
        public string MassageSenderId { get; set; }

        /// <summary>
        /// Message text.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Date of message creation.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// One-to-many relation with Chat entity.
        /// </summary>
        public Chat Chat { get; set; }

        /// <summary>s
        /// One-to-many relation with MessageComplaint entity. 
        /// </summary>
        /// <remarks>
        /// Represent list om message complaints.
        /// </remarks>
        public ICollection<MessageComplaint> MessageComplaints { get; set; }

        /// <summary>
        /// Constructor set list of message complaints.
        /// </summary>
        public Message()
        {
            MessageComplaints = new List<MessageComplaint>();
        }
    }
}
