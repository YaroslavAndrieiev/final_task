﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    /// <summary>
    /// User profile data base entity.
    /// </summary>
    public class UserProfile
    {
        /// <summary>
        /// Primary key. User profile id.
        /// </summary>
        public string UserProfileId { get; set; }

        /// <summary>
        /// User real name.
        /// </summary>
        /// <remarks>
        /// Contains as first name as the last name.
        /// </remarks>
        public string Name { get; set; }

        /// <summary>
        /// User birthday.
        /// </summary>
        public DateTime Birthday { get; set; }

        /// <summary>
        /// User phone number.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// User status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// One-to-one relation with MessengerUser entity.
        /// </summary>
        public virtual MessengerUser MessengerUser { get; set; }
    }
}
