﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    /// <summary>
    /// Available user roles in system.
    /// </summary>
    public enum RoleTypes
    {
        Admin,
        User
    }
}
