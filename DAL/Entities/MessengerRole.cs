﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    /// <summary>
    /// MessengerRole entity.
    /// </summary>
    /// <remarks>
    /// Inherits IdentityRole.
    /// </remarks>
    public class MessengerRole : IdentityRole
    {
    }
}
