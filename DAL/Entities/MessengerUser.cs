﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    /// <summary>
    /// MessengerUser entity.
    /// </summary>
    /// <remarks>
    /// Inherits IdentityUser.
    /// </remarks>
    public class MessengerUser : IdentityUser
    {
        /// <summary>
        /// One-to-one relation with UserProfile entity.
        /// </summary>
        public virtual UserProfile UserProfile { get; set; }

        /// <summary>
        /// State of ban.
        /// </summary>
        /// <remarks>
        /// True - user baned.
        /// False(default) - user not banned.
        /// </remarks>
        public bool BanState { get; set; }
    }
}
