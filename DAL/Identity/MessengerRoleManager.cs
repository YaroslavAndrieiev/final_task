﻿using DAL.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Identity
{
    public class MessengerRoleManager : RoleManager<MessengerRole>
    {
        public MessengerRoleManager(RoleStore<MessengerRole> store) : base(store)
        { }
    }
}
