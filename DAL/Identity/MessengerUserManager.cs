﻿using DAL.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Identity
{
    public class MessengerUserManager : UserManager<MessengerUser>
    {
        public MessengerUserManager(IUserStore<MessengerUser> store) : base(store)
        { }
    }
}
