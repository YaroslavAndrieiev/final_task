﻿using DAL.EF;
using DAL.Entities;
using DAL.Identity;
using DAL.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    /// <summary>
    /// Unit of work implementation.
    /// </summary>
    public class UnitOfWork : IDisposable, IUnitOfWork<MessengerContext>
    {
        private bool isDisposed = false;
        public MessengerContext context { get; }
        private Dictionary<Type, object> repositories;

        private MessengerUserManager userManager;
        private MessengerRoleManager roleManager;

        /// <summary>
        /// Unit of work construcnot initialize context.
        /// </summary>
        /// <param name="connectionString">Connection string to data base.</param>
        public UnitOfWork(string connectionString)
        {
            context = new MessengerContext(connectionString);
            userManager = new MessengerUserManager(new UserStore<MessengerUser>(context));
            roleManager = new MessengerRoleManager(new RoleStore<MessengerRole>(context));
        }

        /// <summary>
        /// Getter returns MessengerUserManager.
        /// </summary>
        public MessengerUserManager UserManager
        {
            get { return userManager; }
        }

        /// <summary>
        /// Getter returns MessengerRoleManager.
        /// </summary>
        public MessengerRoleManager RoleManager
        {
            get { return roleManager; }
        }

        /// <summary>
        /// Method get repository.
        /// </summary>
        /// <remarks>
        /// <para>If repository doesn't exist method will create it.</para>
        /// <para>If dictionary with repositories doesn't exist method will create it.</para>
        /// </remarks>
        /// <typeparam name="TEntity">Type of repository to get.</typeparam>
        /// <returns>Repository IGenericRepository.</returns>
        public IGenericRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            if (repositories == null)
            {
                repositories = new Dictionary<Type, object>();
            }

            var type = typeof(TEntity);
            if (!repositories.ContainsKey(type))
            {
                repositories[type] = new GenericRepository<TEntity>(context);
            }

            return (IGenericRepository<TEntity>)repositories[type];
        }

        /// <summary>
        /// Method commit database changes async.
        /// </summary>
        public async Task SaveChangesAsync()
        {
            await context.SaveChangesAsync();
        }

        /// <summary>
        /// Method commit database changes.
        /// </summary>
        public void SaveChanges()
        {
            context.SaveChanges();
        }

        /// <summary>
        /// Method disposes of connection to the database and set isDisposed variable value.
        /// </summary>
        public virtual void Dispose(bool disposing)
        {
            if (!this.isDisposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
                this.isDisposed = true;
            }
        }

        /// <summary>
        /// Calling dispose of method and saying to the compiler "this object doesn't need of automatic finalization".
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
