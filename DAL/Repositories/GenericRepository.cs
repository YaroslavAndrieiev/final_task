﻿using DAL.EF;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    /// <summary>
    /// Generic repository implementation.
    /// </summary>
    /// <typeparam name="T">Generic param.</typeparam>
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private MessengerContext context;
        private DbSet<T> table;

        /// <summary>
        /// Constructor set context and create db set.
        /// </summary>
        /// <param name="contextInput">Library data base context.</param>
        public GenericRepository(MessengerContext contextInput)
        {
            context = contextInput;
            table = contextInput.Set<T>();
        }

        /// <summary>
        /// Create method implementation.
        /// </summary>
        /// <param name="item">Item to create.</param>
        /// <exception cref="ArgumentNullException">Will be thown if item is null.</exception>
        public void Create(T item)
        {
            if (item == null)
                throw new ArgumentNullException("item");
            table.Add(item);
        }

        /// <summary>
        /// Delete method implementation.
        /// </summary>
        /// <param name="id">Id of item to delete.</param>
        public void Delete(int id)
        {
            T existingItem = table.Find(id);
            if (existingItem == null)
                throw new ArgumentException("id");
            table.Remove(existingItem);
        }

        /// <summary>
        /// Async get method implementation.
        /// </summary>
        /// <param name="id">Id of item to get.</param>
        /// <returns></returns>
        public async Task<T> GetAsync(int id)
        {
            return await table.FindAsync(id);
        }

        /// <summary>
        /// Get All async implementation.
        /// </summary>
        /// <returns>Db set as IEnumerable(T).</returns>
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await table.ToArrayAsync();
        }

        /// <summary>
        /// Update method implementation.
        /// </summary>
        /// <param name="item">Item to update.</param>
        /// <exception cref="ArgumentNullException">Will be thown if item is null.</exception>
        public void Update(T item)
        {
            if (item == null)
                throw new ArgumentNullException("item");
            table.Attach(item);
            context.Entry(item).State = EntityState.Modified;
        }
    }
}
